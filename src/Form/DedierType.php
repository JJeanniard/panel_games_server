<?php

namespace App\Form;

use App\Entity\Dedier;
use App\Form\DedierIPType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class DedierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)

            ->add('DedierIPs', CollectionType::class, [
                'entry_type' => DedierIPType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => false,
                'by_reference' => false,
                'label' => false,
            ])
            ->add('port')
            ->add('username', TextType::class)
            ->add('password', PasswordType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('system')
            ->add('path')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dedier::class,
        ]);
    }
}
